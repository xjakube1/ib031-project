    simple_mean_imputer_pipeline = make_pipeline(
    SimpleImputer(missing_values=np.nan, strategy="mean"),
)

iterative_imputer_pipeline = make_pipeline(
    IterativeImputer(),
)

knn_imputer_pipeline = make_pipeline(
    KNNImputer(),
)