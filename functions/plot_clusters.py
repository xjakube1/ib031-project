import seaborn as sns

def plot_clusters(data, clusters):
    """
    Plots clusters using scatter plot and color them acordingly.

    :param pd.DataFrame data: dataframe with datapoints havig columns "x" and "y"
    :param list of int clusters: cluster label for each of the datapoint
    """
    # find outliers not belonging to any cluster (only relevant for DBSCAN)
    outlier_indices = clusters == -1

    # plot points and color them by cluster/label
    ax = sns.scatterplot(
        data=data[~outlier_indices],
        x="x",
        y="y",
        hue=clusters[~outlier_indices],
        palette="muted",
        legend=False,
    )

    if outlier_indices.any():
        ax = sns.scatterplot(
            data=data[outlier_indices], x="x", y="y", color="black", ax=ax
        )
    return ax